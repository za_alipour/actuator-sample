package com.alipour.test.gitlabcliwithkubernates;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContinuousDeliveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContinuousDeliveryApplication.class, args);
	}

}
